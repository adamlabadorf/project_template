install.packages(c("tidyr","ggplot"),
  "/usr3/graduate/labadorf/R/x86_64-pc-linux-gnu-library/3.4",
  repos="https://cloud.r-project.org/"
)

source("http://bioconductor.org/biocLite.R")

biocLite("DESeq2")
