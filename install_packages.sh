conda install -c bioconda -c r star fastqc samtools=1.4.1 r bowtie salmon

pip install -r requirements.txt

#Rscript install_R_packages.R

conda list --export > conda_packages.txt
